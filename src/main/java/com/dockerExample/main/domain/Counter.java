package com.dockerExample.main.domain;

import lombok.Data;
import lombok.Getter;

import javax.persistence.*;

@Entity
@Data
@Getter

public class Counter {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private int counter;
}
