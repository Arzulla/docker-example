package com.dockerExample.main.repository;

import com.dockerExample.main.domain.Counter;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface CounterRepository
                    extends JpaRepository<Counter,Integer> {

    @Query("select  c from Counter c where c.id = (select max(c.id) from Counter) ")
    Counter findLastRecord();
}
