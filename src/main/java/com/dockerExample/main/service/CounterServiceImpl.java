package com.dockerExample.main.service;

import com.dockerExample.main.domain.Counter;
import com.dockerExample.main.repository.CounterRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CounterServiceImpl implements CounterService {

    private final CounterRepository counterRepository;

    @Override
    public String increment() {
        Counter counter = counterRepository.findLastRecord();

        if (counter == null) {
            counter = new Counter();
        }

        int currentCounter = counter.getCounter();
        int nextCounter = currentCounter + 1;
        counter.setCounter(nextCounter);
        counterRepository.save(counter);

        return "Hello, the counter is  "+ nextCounter;
    }
}
