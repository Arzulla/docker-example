package com.dockerExample.main.service;

import com.dockerExample.main.domain.Counter;

public interface CounterService {
    String increment();
}
