package com.dockerExample.main.rest;

import com.dockerExample.main.domain.Counter;
import com.dockerExample.main.service.CounterService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class HelloController {

    private final CounterService counterService;

    @GetMapping("/hello")
    public String incremet() {

        return counterService.increment();
    }

    @GetMapping("/test")
    public Counter test() {
        Counter counter = new Counter();
        counter.setId(1);
        counter.setCounter(2);
        ;
        return counter;
    }


}
